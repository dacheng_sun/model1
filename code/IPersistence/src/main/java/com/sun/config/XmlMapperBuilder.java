package com.sun.config;

import com.sun.pojo.Configuration;
import com.sun.pojo.MapperStatement;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.InputStream;
import java.util.List;

/**
 * <p>
 * </p>
 *
 * @author dacheng.sun
 * @version 1.0.0
 * @date 2020/4/25 16:36
 */
public class XmlMapperBuilder {
    private Configuration configuration;
    private String nameSpace;

    public XmlMapperBuilder(Configuration configuration) {
        this.configuration = configuration;
    }

    public void parse(InputStream inputStream) throws DocumentException {
        Document document = new SAXReader().read(inputStream);
        Element rootElement = document.getRootElement();
        nameSpace = rootElement.attributeValue("namespace");
        setMapperStatementMap(rootElement.selectNodes("//select"));
        setMapperStatementMap(rootElement.selectNodes("//insert"));
        setMapperStatementMap(rootElement.selectNodes("//update"));
        setMapperStatementMap(rootElement.selectNodes("//delete"));
    }

    public void setMapperStatementMap(List<Element> list) throws DocumentException {
        for (Element element : list) {
            String id = element.attributeValue("id");
            String resultType = element.attributeValue("resultType");
            String paramterType = element.attributeValue("paramterType");
            String sql = element.getTextTrim();
            MapperStatement mapperStatement = new MapperStatement(id, resultType, paramterType, sql);
            String key = nameSpace + "." + id;
            configuration.getMapperStatementMap().put(key, mapperStatement);
        }
    }
}
