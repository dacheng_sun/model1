package com.sun.io;

import java.io.InputStream;

/**
 * <p>
 * </p>
 *
 * @author dacheng.sun
 * @version 1.0.0
 * @date 2020/4/25 15:45
 */
public class Resource {
    public static InputStream getResourceAsStream(String path) {
        InputStream inputStream = Resource.class.getClassLoader().getResourceAsStream(path);
        return inputStream;
    }
}
