package com.sun.pojo;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * </p>
 *
 * @author dacheng.sun
 * @version 1.0.0
 * @date 2020/4/25 15:58
 */
public class Configuration {

    private DataSource dataSource;

    private Map<String,MapperStatement> mapperStatementMap=new HashMap<>();

    public Configuration() {
    }

    public Configuration(DataSource dataSource, Map<String, MapperStatement> mapperStatementMap) {
        this.dataSource = dataSource;
        this.mapperStatementMap = mapperStatementMap;
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public Map<String, MapperStatement> getMapperStatementMap() {
        return mapperStatementMap;
    }

    public void setMapperStatementMap(Map<String, MapperStatement> mapperStatementMap) {
        this.mapperStatementMap = mapperStatementMap;
    }
}
