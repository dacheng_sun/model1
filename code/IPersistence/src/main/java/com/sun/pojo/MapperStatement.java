package com.sun.pojo;

/**
 * <p>
 * </p>
 *
 * @author dacheng.sun
 * @version 1.0.0
 * @date 2020/4/25 15:58
 */
public class MapperStatement {

    private String id;
    private String resultType;
    private String paramterType;
    private String sql;

    public MapperStatement() {
    }

    public MapperStatement(String id, String resultType, String paramterType, String sql) {
        this.id = id;
        this.resultType = resultType;
        this.paramterType = paramterType;
        this.sql = sql;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    public String getParamterType() {
        return paramterType;
    }

    public void setParamterType(String paramterType) {
        this.paramterType = paramterType;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }
}
