package com.sun.SqlSession;

import java.util.List;

public interface SqlSession {

    public <E> List<E> selectList(String id, Object... params) throws Exception;

    public <T> T selectOne(String id, Object... params) throws Exception;

    public int insert(String id, Object... params) throws Exception;

    public int update(String id, Object... params) throws Exception;

    public int delete(String id, Object... params) throws Exception;

    public <T> T getMapper(Class<?> mapperClass);
}
