package com.sun.SqlSession;

import com.sun.pojo.Configuration;
import com.sun.pojo.MapperStatement;

import java.beans.IntrospectionException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.List;

public interface Executor {
    public <E> List<E> query(Configuration configuration, MapperStatement mapperStatement, Object... params) throws Exception;

    public int update(Configuration configuration, MapperStatement mapperStatement, Object... params) throws SQLException, ClassNotFoundException, IllegalAccessException, InstantiationException, InvocationTargetException, NoSuchFieldException, IntrospectionException, Exception;
}
