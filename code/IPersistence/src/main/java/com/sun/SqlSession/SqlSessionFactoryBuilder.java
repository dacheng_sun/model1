package com.sun.SqlSession;

import com.sun.config.XmlConfigBuilder;
import com.sun.pojo.Configuration;
import org.dom4j.DocumentException;

import java.beans.PropertyVetoException;
import java.io.InputStream;

/**
 * <p>
 * </p>
 *
 * @author dacheng.sun
 * @version 1.0.0
 * @date 2020/4/25 16:10
 */
public class SqlSessionFactoryBuilder {

    public SqlSessionFactory build(InputStream in) throws PropertyVetoException, DocumentException {
        //使用dom4j解析，封装成configuration
        XmlConfigBuilder xmlConfigBuilder = new XmlConfigBuilder();
        Configuration configuration = xmlConfigBuilder.parseConfig(in);

        //创建SqlSessionFactory对象,生产SqlSession
        return new DefaultSqlSessionFactory(configuration);

    }

}
