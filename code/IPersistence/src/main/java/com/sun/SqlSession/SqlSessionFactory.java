package com.sun.SqlSession;

public interface SqlSessionFactory {
    public SqlSession openSession();
}
