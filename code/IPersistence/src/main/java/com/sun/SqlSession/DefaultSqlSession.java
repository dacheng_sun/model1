package com.sun.SqlSession;

import com.sun.pojo.Configuration;

import java.lang.reflect.*;
import java.util.List;

/**
 * <p>
 * </p>
 *
 * @author dacheng.sun
 * @version 1.0.0
 * @date 2020/4/25 16:57
 */
public class DefaultSqlSession implements SqlSession {
    private Configuration configuration;

    public DefaultSqlSession(Configuration configuration) {
        this.configuration = configuration;
    }

    @Override
    public <E> List<E> selectList(String id, Object... params) throws Exception {
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        List<Object> query = simpleExecutor.query(configuration, configuration.getMapperStatementMap().get(id), params);
        return (List<E>) query;
    }

    @Override
    public <T> T selectOne(String id, Object... params) throws Exception {
        List<Object> objects = selectList(id, params);
        if (objects.size() == 1) {
            return (T) objects.get(0);
        } else {
            throw new RuntimeException("结果不唯一");
        }
    }

    @Override
    public int insert(String id, Object... params) throws Exception {
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        return simpleExecutor.update(configuration, configuration.getMapperStatementMap().get(id), params);
    }

    @Override
    public int update(String id, Object... params) throws Exception {
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        return simpleExecutor.update(configuration, configuration.getMapperStatementMap().get(id), params);
    }

    @Override
    public int delete(String id, Object... params) throws Exception {
        SimpleExecutor simpleExecutor = new SimpleExecutor();
        return simpleExecutor.update(configuration, configuration.getMapperStatementMap().get(id), params);
    }

    @Override
    public <T> T getMapper(Class<?> mapperClass) {
        //利用动态代理生成代理对象
        Object o = Proxy.newProxyInstance(DefaultSqlSession.class.getClassLoader(), new Class[]{mapperClass}, new InvocationHandler() {
            @Override
            public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                String methodName = method.getName();
                String className = method.getDeclaringClass().getName();
                String id = className + "." + methodName;
                //获取方法的返回值类型
                Type genericReturnType = method.getGenericReturnType();

                if ("int".equals(genericReturnType.getTypeName())) {
                    switch (methodName){
                        case "insert":
                            return insert(id, args);
                        case "update":
                            return update(id, args);
                        case "delete":
                            return delete(id, args);
                    }
                }

                if (genericReturnType instanceof ParameterizedType) {
                    System.out.println("genericReturnType = " + genericReturnType);
                    return selectList(id, args);
                } else {
                    return selectOne(id, args);
                }
            }
        });
        return (T) o;
    }
}

