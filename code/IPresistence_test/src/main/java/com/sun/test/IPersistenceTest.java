package com.sun.test;

import com.sun.SqlSession.SqlSession;
import com.sun.SqlSession.SqlSessionFactory;
import com.sun.SqlSession.SqlSessionFactoryBuilder;
import com.sun.dao.UserMapper;
import com.sun.io.Resource;
import com.sun.pojo.User;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

/**
 * <p>
 * </p>
 *
 * @author dacheng.sun
 * @version 1.0.0
 * @date 2020/4/25 15:50
 */
public class IPersistenceTest {

    private UserMapper userMapper;

    @Before
    public void before() throws Exception {
        InputStream resourceAsStream = Resource.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory build = new SqlSessionFactoryBuilder().build(resourceAsStream);
        SqlSession sqlSession = build.openSession();
        userMapper = sqlSession.getMapper(UserMapper.class);
    }


    @Test
    public void selectTest() throws Exception {
        User user = new User();
        user.setId(3);
        user.setUsername("lucas");
        User user1 = userMapper.selectOne(user);
        System.out.println("user1 = " + user1);
    }

    @Test
    public void selectListTest() throws Exception {
        List<User> users = userMapper.selectList();
        for (User result : users) {
            System.out.println("result = " + result);
        }
    }

    @Test
    public void insertTest() {
        User user = new User();
        user.setId(3);
        user.setUsername("lucas");
        user.setPassword("123");
        user.setBirthday("2019-12-12");
        System.out.println(userMapper.insert(user));
    }

    @Test
    public void updateTest() {
        User user = new User();
        user.setId(3);
        user.setUsername("louis");
        System.out.println(userMapper.update(user));
    }

    @Test
    public void deleteTest() {
        System.out.println(userMapper.delete(3));
    }
}
