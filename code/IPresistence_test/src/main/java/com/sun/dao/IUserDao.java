package com.sun.dao;

import com.sun.pojo.User;

import java.util.List;

/**
 * <p>
 * </p>
 *
 * @author dacheng.sun
 * @version 1.0.0
 * @date 2020/4/25 15:32
 */
public interface IUserDao {

    public User selectOne(User user) throws Exception;

    public List<User> selectList() throws Exception;
}
