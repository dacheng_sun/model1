//package com.sun.dao;
//
//import com.sun.SqlSession.SqlSession;
//import com.sun.SqlSession.SqlSessionFactory;
//import com.sun.SqlSession.SqlSessionFactoryBuilder;
//import com.sun.io.Resource;
//import com.sun.pojo.User;
//
//import java.io.InputStream;
//import java.util.List;
//
///**
// * <p>
// * </p>
// *
// * @author dacheng.sun
// * @version 1.0.0
// * @date 2020/4/25 18:35
// */
//public class IUserDaoImp implements IUserDao {
//    @Override
//    public User selectOne(User user) throws Exception {
//        InputStream resourceAsStream = Resource.getResourceAsStream("SqlMapConfig.xml");
//        SqlSessionFactory build = new SqlSessionFactoryBuilder().build(resourceAsStream);
//        SqlSession sqlSession = build.openSession();
//        User result = sqlSession.selectOne("user.selectOne", user);
//        System.out.println("result = " + result);
//        return result;
//    }
//
//    @Override
//    public List<User> selectList() throws Exception {
//        InputStream resourceAsStream = Resource.getResourceAsStream("SqlMapConfig.xml");
//        SqlSessionFactory build = new SqlSessionFactoryBuilder().build(resourceAsStream);
//        SqlSession sqlSession = build.openSession();
//        List<User> users = sqlSession.selectList("user.selectList");
//        for (User result : users) {
//            System.out.println("result = " + result);
//        }
//        return users;
//    }
//}
